package edu.sjsu.android.hw5;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    Button button1, button2, button3, button4;
    ImageView image;
    ProgressDialog progress;
    EditText editText;

    int downloadDone = 0;
    Bitmap globalBitmap=null;
    URL url = null;
    URL finalURL;

    // Handler myHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {
            url = new URL("https://upload.wikimedia.org/wikipedia/commons/a/ab/Apple-logo.png");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }


        finalURL = url;
        image = (ImageView) findViewById(R.id.image);
        editText = (EditText) findViewById(R.id.editText);

        button1 = findViewById(R.id.button1);
        button2 = findViewById(R.id.button2);
        button3 = findViewById(R.id.button3);
        button4 = findViewById(R.id.button4);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String userURL = (String) editText.getText().toString();
                try {
                    if(userURL.compareTo("") != 0)
                        url = new URL(userURL);
                    else
                        url = new URL("https://upload.wikimedia.org/wikipedia/commons/a/ab/Apple-logo.png");

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                finalURL = url;

                runRunnable();
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String userURL = (String) editText.getText().toString();
                try {
                    if(userURL.compareTo("") != 0)
                        url = new URL(userURL);
                    else
                        url = new URL("https://upload.wikimedia.org/wikipedia/commons/a/ab/Apple-logo.png");

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                finalURL = url;

                runMessages();
            }
        });


        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String userURL = (String) editText.getText().toString();
                try {
                    if(userURL.compareTo("") != 0)
                        url = new URL(userURL);
                    else
                        url = new URL("https://upload.wikimedia.org/wikipedia/commons/a/ab/Apple-logo.png");

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                finalURL = url;

                new asyncImage().execute(finalURL);
            }
        });

        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                image.setImageBitmap(null);
            }
        });
    }


    protected void runRunnable() {

        progress = new ProgressDialog(MainActivity.this);
        progress.setTitle("HW5");
        progress.setMessage("Downloading...");
        progress.setIndeterminate(false);
        progress.show();

        // create-start background thread where the busy work will be done
        Thread myBackgroundThread = new Thread( backgroundTask, "backAlias1" );
        myBackgroundThread.start();
    }

    private Runnable foregroundRunnable = new Runnable() {
        @Override
        public void run() {
            try {

                if(downloadDone == 1) {
                    // Set image here
                    image.setImageBitmap(globalBitmap);

                    progress.dismiss();
                    downloadDone=0;
                    globalBitmap=null;

                }

        } catch (Exception e) {
            Log.e("<<foregroundTask>>", e.getMessage()); }
        }

    }; // foregroundTask

    private Runnable backgroundTask = new Runnable() {
        @Override
        public void run() {

            System.out.println("In backgroundTask..");

            // Download image
            globalBitmap = downloadBitmap(url);

            downloadDone = 1;

            handler.post(foregroundRunnable);
        }

    };// backgroundTask

    public Bitmap downloadBitmap(URL myURL) {

        Bitmap myBitmap = null;
        HttpURLConnection urlC = null;

        try {

            //Artifical delay
            Thread.sleep(5000);

            urlC = (HttpURLConnection) myURL.openConnection();

            InputStream in = new BufferedInputStream(urlC.getInputStream());
            myBitmap = BitmapFactory.decodeStream(in);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            urlC.disconnect();
        }

        return myBitmap;

    }


    Handler handler = new Handler() {

        @SuppressLint("HandlerLeak")

        @Override
        public void handleMessage(Message msg) {

            System.out.println("In handleMessage...");

            Integer bgDone = (Integer)msg.obj;

            //do something with the value sent by the background thread here
            {
                image.setImageBitmap(globalBitmap);
                progress.dismiss();
                globalBitmap=null;
            }
            // else
            // System.out.println("Msg Recd: " + bgDone);

        }
    }; //handler

    protected void runMessages() {

        progress = new ProgressDialog(MainActivity.this);
        progress.setTitle("HW5");
        progress.setMessage("Downloading...");
        progress.setIndeterminate(false);
        progress.show();

        System.out.println("runMessages. Going to start bg thread...");

        // create-start background thread where the busy work will be done
        Thread background = new Thread(new Runnable() {

            @Override
            public void run() {
                try {

                    System.out.println("runMessages. Going to Download");

                    globalBitmap = downloadBitmap(url);

                    Message msg = handler.obtainMessage(1, (Integer)1);
                    handler.sendMessage(msg);

                    System.out.println("runMessages. Message sent");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        background.start();
    }

    private class asyncImage extends AsyncTask {

        @Override
        protected Object doInBackground(Object[] objects) {

            // System.out.println("In Background Work");

            return (downloadBitmap((java.net.URL)objects[0]));
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            // System.out.println("In PreExecute");

            progress = new ProgressDialog(MainActivity.this);
            progress.setTitle("HW5");
            progress.setMessage("Downloading...");
            progress.setIndeterminate(false);
            progress.show();
        }

        public asyncImage() {
            super();
        }

        @Override
        protected void onPostExecute(Object result) {

            image.setImageBitmap((Bitmap)result);

            progress.dismiss();
        }
    }
}